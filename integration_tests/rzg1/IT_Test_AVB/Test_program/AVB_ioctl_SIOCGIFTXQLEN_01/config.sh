#!/bin/sh
num=`ifconfig  | egrep "^eth" | awk '{print $1}' | wc -l`
if [ "$num" == "1" ]
then
	avb_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '1p'`
	if [ "$avb_device" != "" ]
	then
		echo "AVB_DEV = $avb_device" >> board_config.txt
	fi
	echo "ETH_DEV = dont_exist" >> board_config.txt
fi


if [ "$num" == "2" ]
then
	avb_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '2p'`
	if [ "$avb_device" != "" ]
	then
		echo "AVB_DEV = $avb_device" >> board_config.txt
	fi

	eth_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '1p'`
	if [ "$eth_device" != "" ]
	then
		echo "ETH_DEV = $eth_device" >> board_config.txt
	fi
fi


if [ "$num" == "3" ]
then
	avb_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '3p'`
	if [ "$avb_device" != "" ]
	then
		echo "AVB_DEV = $avb_device" >> board_config.txt
	fi

	eth_device=`ifconfig  | egrep "^eth" | awk '{print $1}' | sort | sed -n '2p'`
	if [ "$eth_device" != "" ]
	then
		echo "ETH_DEV = $eth_device" >> board_config.txt
	fi
fi
