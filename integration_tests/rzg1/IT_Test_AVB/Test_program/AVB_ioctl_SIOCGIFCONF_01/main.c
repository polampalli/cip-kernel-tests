/*
 * Project: LAVA
 * Test ID: test_case
 * Feature: Checking ioctl_SIOCGIFCONF system call
 * Sequence: socket();ioctl_SIOCGIFCONF()
 * Testing level: system call
 * Test-case type: Normal
 * Expected result: OK
 * Name: main.c
 * Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: Condition: Call ioctl_SIOCGIFCONF with device eth1 after call socket with input AF_INET; SOCK_STREAM; IPPROTO_TCP. Expected result = OK
 */
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	struct ifconf ifconf_dev;
	//	struct ifreq	*ifreq_dev;
	memset(&ifconf_dev, 0, sizeof(ifconf_dev));
	int i = 0;
	//Call set of functions under test
	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	result = ioctl(fd, SIOCGIFCONF, &ifconf_dev);
	//	printf("ifconf_dev.ifc_len %d\n",ifconf_dev.ifc_len);
	//	ifreq_dev = (struct ifreq *)&(ifconf_dev.ifc_req);
	//	while(i < ifconf_dev.ifc_len){
	//           	printf("address of ifconf_dev %d - ifreq_dev ->  %p\n", i, ifreq_dev);
	//		i++;
	//	}
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unkonw_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

